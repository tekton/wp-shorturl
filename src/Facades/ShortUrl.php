<?php namespace Tekton\Wordpress\ShortUrl\Facades;

class ShortUrl extends \Tekton\Support\Facade {
    protected static function getFacadeAccessor() { return 'wp.shorturl'; }
}
