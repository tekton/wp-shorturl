<?php namespace Tekton\Wordpress\ShortUrl\Providers;

use Tekton\Support\ServiceProvider;
use Tekton\Wordpress\ShortUrl\ShortUrlManager;

class ShortUrlProvider extends ServiceProvider {

    function register() {
        $this->app->singleton('wp.shorturl', function() {
            return new ShortUrlManager();
        });
    }

    function boot() {
        // Disable url guessing
        add_filter('redirect_canonical', function ($redirect_url) {
            return (is_404()) ? false : $redirect_url;
        });

        // Redirect if shorturl is defined
        add_action('template_redirect', function () {
            $shorturl = app('wp.shorturl');

            if ($shorturl->shouldRedirect()) {
                $shorturl->redirect();
            }

            return false;
        });

        // Add cmb2 metaboxes
        add_action('metabox_init', function () {
            $short_url = create_metabox( array(
                'id'            => 'short_url',
                'title'         => __( 'Short URL', 'tekton-wp-shorturl' ),
                'object_types'  => apply_filters('shorturl_post_types', app('wp.shorturl')->defaultPostTypes()), // Post type
                'context'       => 'normal',
                'priority'      => 'default',
                'show_names'    => true, // Show field names on the left
            ));

            $page_handle = $short_url->add_field( array(
                'name' => __( 'Page Handle', 'tekton-wp-shorturl' ),
                'id'   => meta_key('global', 'short_url'),
                'type' => 'text_small',
                'desc' => 'The short url is a case insensitive alpha-numeric string.',
                'sanitization_cb' => 'sanitize_key',
            ));

            if (is_admin_edit()) {
                $grid = create_grid($short_url);
                $row = $grid->addRow();
                $row->addColumns(array($page_handle));
            }
        });

        // Clear cache on post saves
        add_action('save_post', function() {
            app('cache')->forget('short_urls');
        });
    }
}
