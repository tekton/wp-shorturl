<?php namespace Tekton\Wordpress\ShortUrl;

use Tekton\Wordpress\Post;

class ShortUrlManager {

    protected $cache;
    protected $urlMap = [];
    protected $postTypes = ['page', 'post'];

    function __construct() {
        $this->cache = app('cache');

        // Load URLs
        $this->urlMap = $this->cache->rememberForever('short_urls', function() {
            $short_urls = [];

            $posts = get_posts([
                'post_type' => apply_filters('shorturl_post_types', $this->defaultPostTypes()),
                'fields' => 'ids',
                'nopaging' => true,
                'meta_query' => array(
                   array(
                       'key' => meta_key('global', 'short_url'),
                       'compare' => 'EXISTS'
                   ),
               ),
            ]);

            if ( ! empty($posts)) {
                foreach ($posts as $post_id) {
                    $post = new Post($post_id);
                    $short_urls[strtolower($post->short_url)] = $post->url;
                }
            }

            return $short_urls;
        });
    }

    function defaultPostTypes() {
        return $this->postTypes;
    }

    function has($key) {
        return (isset($this->urlMap[$key])) ? true : false;
    }

    function get($key, $default = null) {
        return (isset($this->urlMap[$key])) ? $this->urlMap[$key] : $default;
    }

    function currentHandle() {
        global $wp_query;

        if ( ! $wp_query->is_404) {
            //This is a valid URL of WordPress!
            return false;
        }

        // Get page name
        return (empty($wp_query->query['pagename'])) ? false : strtolower($wp_query->query['pagename']);
    }

    function redirectUrl() {
        return $this->get($this->currentHandle());
    }

    function shouldRedirect() {
        return ($this->has($this->currentHandle())) ? true : false;
    }

    function redirect() {
        if ($this->shouldRedirect()) {
            wp_redirect($this->redirectUrl(), 301);
            exit;
        }
    }
}
