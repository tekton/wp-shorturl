Tekton WP-ShortURL
==================

A simple component that adds a Short URL metabox to Wordpress posts that lets you manually define a short link that would redirect to the post.
